class Rover
  attr_accessor :position_x, :position_y, :orientation

  ORIENTATION_MAPPING = { 'N' => 0, 'E' => 90, 'S' => 180, 'W' => 270 }

  def initialize(position_x, position_y, orientation)
    @position_x = position_x
    @position_y = position_y
    @orientation = orientation
  end

  def move(instructions)
    instructions.scan(/L|R|M/).each do |step|
      case step
      when 'L'
        spin(-90)
      when 'R'
        spin(90)
      when 'M'
        forward
      end
    end
    # Method assumes move is possible (no falling of or going outside, just exploring)
    [position_x, position_y, orientation].join(' ')
  end

  private

  def spin(n)
    orientation_value = (ORIENTATION_MAPPING[orientation] + n + 360) % 360
    self.orientation = ORIENTATION_MAPPING.key(orientation_value)
  end

  def forward
    case orientation
    when 'N'
      self.position_y += 1
    when 'S'
      self.position_y -= 1
    when 'E'
      self.position_x += 1
    when 'W'
      self.position_x -= 1
    end
  end
end
