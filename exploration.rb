require_relative 'lib/board'
require_relative 'lib/rover'

p '************************************************'
p ' -- Control panel of Mars Exploration Rovers -- '

max_coordinates = []
while max_coordinates.empty? do
  p 'What are maximum upper-right coordinates of the plateau: (ex: 5 5)'
  max_coordinates = gets.split(' ').
    select { |item| item.to_i.to_s == item }

  p 'Please input values as shown in example' unless max_coordinates.size == 2
end

board = Board.new(*max_coordinates)
p "Board size is: #{board.max_x}x#{board.max_y}"

p '---------'
puts "Please input instructions for rovers.\n
 Each rover has two lines of input. \n
 The first line gives the rover's position (ex: 4 5 N), \n
 and the second line is a series of instructions
 telling the rover how to explore the plateau (ex: LMLMMMRMRM)."
p '---------'

p "Rover 1: "
position_x_1, position_y_1, orientation_1 = gets.chomp.split(' ')
instructions_rover_1 = gets.chomp

rover_1 = Rover.new(position_x_1.to_i, position_y_1.to_i, orientation_1)
rover_1.move(instructions_rover_1)

p 'Rover 2: '
position_x_2, position_y_2, orientation_2 = gets.chomp.split(' ')
instructions_rover_2 = gets.chomp

rover_2 = Rover.new(position_x_2.to_i, position_y_2.to_i, orientation_2)
rover_1.move(instructions_rover_1)

p 'Rovers are starting their exploartion'
5.times do |n|
  sleep(0.5)
  print '.'
end

p 'Mission suceeded!'
p "Current rover 1 position: #{rover_1.position_x}, #{rover_1.position_y} #{rover_1.orientation}"
p "Current rover 2 position: #{rover_2.position_x}, #{rover_2.position_y} #{rover_2.orientation}"
p '---------'
p 'Congratulations!'
