require_relative '../lib/board'

describe Board do
  describe '.initialize' do
    let(:board) { Board.new(5, 5) }

    it 'sets max x coordinate' do
      expect(board.max_x).to eq 5
    end

    it 'sets max y coordinate' do
      expect(board.max_y).to eq 5
    end
  end
end
