require_relative '../lib/rover'

describe Rover do
  describe '.initialize' do
    let(:rover) { Rover.new(1, 2, 'N') }

    it 'sets x coordinate' do
      expect(rover.position_x).to eq 1
    end

    it 'sets y coordinate' do
      expect(rover.position_y).to eq 2
    end

    it 'sets rover orientation' do
      expect(rover.orientation).to eq 'N'
    end
  end

  describe '#move' do
    context 'test case rover 1' do
      let(:rover) { Rover.new(1, 2, 'N') }

      it 'returns new position of the rover' do
        expect(rover.move('LMLMLMLMM')).to eq '1 3 N'
      end
    end

    context 'test case rover 2' do
      let(:rover) { Rover.new(3, 3, 'E') }

      it 'returns new position of the rover' do
        expect(rover.move('MMRMMRMRRM')).to eq '5 1 E'
      end
    end
  end
end
